# Porjet fils rouge

Dans cette seconde partie du projet fils, l’attention a été portée sur la conteneurisation de l’application Vapormap. Bien qu’ayant déjà eu l’occasion de travailler avec la solution Docker au cours de mes précédents stages et projets, j’ai eu l’occasion durant ces trois jours de projet de découvrir de nouvelles facettes de cette technologie DevOps notamment concernant la minimalisation de la taille des images ou encore le multi-staging.

## Confusion sur le rôle du service NGINX

Le premier élément sur lequel je souhaite revenir dans ce rapport est la confusion que j’ai réalisée lors de la conteneurisation de l’application en production. En effet, le serveur python utilisé en pré-production afin de délivrer les fichiers statiques de l’application frontend a été remplacé en production par un serveur NGINX. Venant du monde du réseau, j’ai beaucoup utilisé NGINX dans les différents projets en tant que proxy. Ainsi, je n’ai pas compris pourquoi il fallait exposer et associer le port 8000 du conteneur backend avec le port 8000 de l'hôte afin que le frontend puisse communiquer avec lui. Je pensais en effet, que le service NGINX du frontend servait de proxy pour le serveur backend en plus d’être le serveur web délivrant les fichiers statiques. Or ici, le serveur NGINX ne sert que de serveur web et non de proxy. Les deux services frontend et backend sont donc indépendants et ne forment pas de stack. La question peut donc se porter sur l’utilité de réaliser un docker-compose et donc un réseau entre deux services qui ne communiquent pas directement entre eux. En effet, il serait tout à fait possible de lancer d’un côté le container du frontend et de l’autre là stack backend/bdd (via un docker-compose). Une autre solution, celle que j’ai retenue, est de bien mettre les services frontend, backend et bdd dans un même docker-compose mais d’y rajouter un dernier service : un proxy NGINX… Ici, NGINX va donc permettre de rediriger les requêtes du frontend vers le backend. Dans ce cas de figure, seuls les ports de NGINX sont ouverts vers l’extérieur et mappés sur les ports de l'hôte.

![](image/diagram.png)

Cependant, pour réaliser cette architecture, il est nécessaire d’effectuer quelques modifications :

* Au niveau du frontend, if faut modifier le fichier config.json.template afin qu'il redirige vers la bonne adresse du backend, le format à deux variables VAPORMAP_HOST et VAPORMAP_PORT ne pouvant plus fonctionner à cause de l'ajout de l'extension de chemin /backend.
* Au niveau du backend, il faut charger lors du build de l’image un nouveau fichier urls.py comprenant les nouvelles extensions d’url. En effet toutes les requêtes en /backend seront redirigées automatiquement vers le serveur backends, alors que les autres seront redirigées par defaut vers le serveur frontend par le proxy.

Enfin, il est nécessaire de configurer correctement le nouveau service NGINX dans notre docker-compose.yaml de production. Au final, nous obtenons le docker-compose suivant. Grace à cette structure, nous économisons une adresse IP flottante tout en masquant la structure de notre réseau.

````yamlfile
version: "3.9"

services:

db:
image: mariadb
restart: always
env_file:
- db.env

backend:
build: ./backend
image: backend
restart: always
env_file:
- backend.env

frontend:
build: ./frontend
image: frontend
restart: always
env_file:
- frontend.env

proxy:
image: nginx
restart: always
volumes:
- ./nginx/nginx.conf:/etc/nginx/nginx.conf
ports:
- 8000:80
env_file:
- nginx.env


````

## Réduction de la taille des images Docker


Lors du build des différentes images je me suis rendu compte que le processus était assez lent et que les images obtenues étaient souvent assez lourdes. Ainsi, pour le backend notamment, l’image initiale construite à partir du Dockerfile suivant faisait quelques 975 MB. Ce qui est plutôt lourd et rend donc le déploiement de l’image sur les serveurs de production assez lent. Je me suis donc intéressé, avec l’aide notamment d’Ali, à une manière de réduire le plus possible la taille de cette image. Cette démarche est très intéressante car elle m’a permis de prendre conscience de l’importance de plein d’éléments propres à la conteneurisation.

Ainsi, la première chose dont il faut faire attention est l’image de base à partir de laquelle nous allons construire notre propre image. En effet, je suis parti avec l’image python-3.9. Cette image contient toous les outils nécéssaires et recommandés pour coder en python y compris des librairies dont je n’ai pas besoin. Par conséquence son poids est de 338 MB. Par ailleurs, il existe d’autres images de python plus légères car ne contenant que le strict minimum. Ainsi, parmi ces images nous retrouvons python:3.9-slim et python:3.9-alpine qui ne font respectivement que 45 et 17 MB. Cependant attention, si ces images sont avantageuses au niveau du poids, elles peuvent également avoir des désavantages. Ainsi ici, nous n’allons pas utiliser l’image python:3.9-alpine car elle ne possède pas git de pré-installer et ne supporte pas la plupart des wheels pythons, ceux-ci n'ayant pas été developpées pour l'OS alpine (chose qui devraient changer dans un avenir proche).

Ensuite, un deuxième point sur lequel nous pouvons travailler pour réduire la taille de notre image est le nombre de layers qui la compose. En effet, chaque commande docker RUN, ADD, ou COPY écrite dans notre Dockerfile va venir créer une nouvelle layer. Or une manière de réduire la taille de notre image est de réduire le nombre layer en regroupant les commandes docker similaires. Ainsi, au lieu d’écrire une commande RUN pour chacune des commandes shell que nous souhaitons lancer à la suite, nous pouvons les regrouper dans une seulement commande RUN. Il est possible (et recommander) de maintenir la lisibilité de notre Dockerfile en réalisant des retours à chacune des commandes avec le &\. La diminution du nombre de layer ne fait cependant pas drastiquement baisser la taille de notre image. En effet, cette opération ne nous a permis de sauver que 2M (ce qui fait quand même beaucoup par rapport à ce qui a été utilisé pour envoyer un homme sur la lune).

Il est également possible de réduire la taille de notre image en rajoutant certains flags notamment lors de l’installation des libraires python avec `–no-cache-dir` qui évite la sauvegarde des dépendances.On peut également remarquer la présence de la variable d'envrionnement `PYTHONDONTWRITEBYTECODE ` definie à 1. Ce flag empèche l'interpréteur python de générer des fichiers .pyc (les fichiers python compiliés) qui ne sont pas utiles dans notre cas (nous utilisons ici l'interpreteur python pour exécuter le code).

Il est également possible d’optimiser l’installation de dépendances apt en ajoutant le flag –no-install-recommends qui empêche l’installation automatique de paquets additionnels recommandés mais non nécessaires au fonctionnement de l’application. Cependant, il faut rester vigilent car l’ajout de ce flag peut entraîner des conséquences sur le bon fonctionnement et les performances de l’application. De même, une fois nos paquets apt installés, il est recommandé de lancer la commande `rm -rf /var/lib/apt/lists/*` qui permet de vider le cache apt. Attention là encore à bien lancer la commande dans la même instruction RUN que celle dans laquelle ont été installée les paquets apt. Toutes ces commandes et flags nous permettent d’abaisser la taille de notre image d’une dizaine de méga-octets.

## Multi-staging

Ce TP a également été l'occasion pour moi de tester le multi-staging. Cette methode consiste à construire notre image finale à partir du contenu d'autres images que nous aurons construites avant. Dans le cadre de python, qui est un language interprété, cette fonctionnalité reste limitée mais dans le cas de languages compilés (soit le code source soit des dépendances), il peut être interressant de compiler le code source dans une première image puis d'utiliser le code compilé dans l'image finale. Cela permet notamment de diminuer la taille de l'image finale qui ne correspondra qu'à la dernière image du staging.

Dans le cas de python, il est possible d'utiliser du multi-staging pour reduire la taille du conteneur final. Pour cela, il suffit d'utiliser la librairie [Pyinstall](https://pypi.org/project/pyinstaller/) qui va venir packager une application python et toutes ses dépendances dans un seul package. Une fois ce package formé, il peut être envoyé dans un autre stage qui ne nécéssitera pas d'interpréteur python et l'installation des libraires pour être exécuté réduisant ainsi la taille finale de notre image.

Pour ma part, je ne suis pas allé jusque là dans la réduction de la taille de mon image Backend. Cependant, avec les précédentes modifications, notre image Pyhton est passée d'une taille approchant le giga-octet à une image de 467 MB.